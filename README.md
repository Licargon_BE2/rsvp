RSVP project made for the "Telecommunications systems" course at University of Antwerp, 3rd Bachelor Year. Made in collaboration with Nicolas Demarbaix.

Project is supposed to be run using Click (found at http://www.read.cs.ucla.edu/click/click). Repository contains all scripts and elements to compile & run the virtual network.