iptos::SetIPTos

RatedSource(DATASIZE 83, RATE 3)
	-> DynamicUDPIPEncap(192.168.1.1, 1234, 192.168.2.1, 1001) 
	-> EtherEncap(0x0800, 00:50:BA:85:84:D1, 00:50:BA:85:84:D1) /// The MAC addresses here shoudl be from the client11  to get past the HostEtherFilter. This way we can reuse the input from the network for the applications.
	-> IPPrint("client -- sent QoS packet")
	-> counterSrcQoS::AverageCounter
	-> [0] iptos

RatedSource(DATASIZE 83, RATE 10)
	-> DynamicUDPIPEncap(192.168.1.1, 1234, 192.168.2.1, 1000) 
	-> EtherEncap(0x0800, 00:50:BA:85:84:D1, 00:50:BA:85:84:D1) /// The MAC addresses here shoudl be from the client11  to get past the HostEtherFilter. This way we can reuse the input from the network for the applications.
	-> IPPrint("client -- sent BE packet")
	-> counterSrcBE::AverageCounter
	-> [0] iptos
	
iptos [0]
	-> SetIPChecksum
	-> ToDump(setiptos.dump)
