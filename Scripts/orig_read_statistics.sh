# Shell script to connect to a Click router on a fixed port and read some statistics
# Authors: Bart Braem and Michael Voorhaen
# Copyright PATS - UA

PORT=10000

echo "Connecting to localhost on port $PORT:"

echo "> Resetting counters"
(echo "write counterDstBE.reset"; sleep 0.5; echo "write counterDstQoS.reset"; sleep 0.5) | telnet localhost $PORT &>/dev/null

echo "> Retrieving statistics"
sleep 2
results=`(echo "read counterSrcBE.byte_rate"; sleep 0.5; echo "read counterSrcQoS.byte_rate"; sleep 0.5; echo "read counterDstBE.byte_rate"; sleep 0.5; echo "read counterDstQoS.byte_rate"; sleep 0.5) | telnet localhost $PORT 2>/dev/null | grep -E "^[0-9]+\.[0-9]+" | cut -f 1 -d " "`

set $results
result=$1
BESRC=$result;
shift                               
result=$1                           
QOSSRC=$result;
shift                               
result=$1                           
BEDST=$result
shift                               
result=$1                           
QOSDST=$result

echo "                SRC                             DESTINATION"
echo "QOS             $QOSSRC                $QOSDST"
echo "BE              $BESRC                $BEDST"