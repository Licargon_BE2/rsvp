sender::RSVPSender(SOURCE 10.0.0.1, DEST 10.0.0.2)
    -> IPEncap(46, 10.0.0.1, 10.0.0.2)
    -> EtherEncap(0x0800, 1:1:1:1:1:1, 2:2:2:2:2:2)
    -> ProcessPath
    -> ToDump(sender.dump) 
    -> Discard
    
responder::RSVPResponder(SOURCE 10.0.0.2, DEST 10.0.0.1)
    -> IPEncap(46, 10.0.0.2, 10.0.0.1)
    -> EtherEncap(0x0800, 2:2:2:2:2:2, 1:1:1:1:1:1)
    -> ProcessResv
    -> ToDump(responder.dump) 
    -> Discard

error::RSVPError
    -> IPEncap(46, 10.0.0.2, 10.0.0.1)
    -> EtherEncap(0x0800, 2:2:2:2:2:2, 1:1:1:1:1:1)
    -> ProcessError
    -> ToDump(error.dump) 
    -> Discard