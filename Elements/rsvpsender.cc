//
//  rsvp_path.cc
//  
//
//  Created by Nicolas Demarbaix on 02/11/13.
//
//

#include <click/config.h>
#include <click/confparse.hh>
#include <click/element.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>

#include <stdlib.h>
#include <time.h>

#include "rsvpsender.hh"

CLICK_DECLS

int RSVPSender::configure(Vector<String> &conf, ErrorHandler *errh) {
    _port = 0;
    if (cp_va_kparse(conf, this, errh, "SOURCE", cpkM, cpIPAddress, &_src, "PORT", cpkN, cpInteger, &_port, cpEnd) < 0)
        return -1;
    _pathTimeout = new Timer(this);
    _pathTimeout->initialize(this);
    return 0;
}

int RSVPSender::initialize(ErrorHandler *errh){
}

int RSVPSender::path(const String &conf, Element *e, void *thunk, ErrorHandler *errh) {
    RSVPSender *me = (RSVPSender*) e;
    IPAddress dest;
    int port;
    if (cp_va_kparse(conf, me, errh, "DEST", cpkM, cpIPAddress, &dest, "PORT", cpkM, cpInteger, &port, cpEnd) < 0)
        return -1;
    me->create_path_message(dest, port);
    return 0;
}

int RSVPSender::path_tear(const String &conf, Element *e, void *thunk, ErrorHandler *errh) {
    RSVPSender *me = (RSVPSender*) e;
    IPAddress dest;
    int port;
    if (cp_va_kparse(conf, me, errh, "DEST", cpkM, cpIPAddress, &dest, "PORT", cpkM, cpInteger, &port, cpEnd) < 0)
        return -1;
    
    me->create_path_tear_message(dest, port);
    return 0;
}

void RSVPSender::run_timer(Timer* t){
    create_path_message(this->_dst, this->_port);
}

void RSVPSender::add_handlers() {
    add_write_handler("path", &path, (void*)0);
    add_write_handler("tear", &path_tear, (void*)0);
}

void RSVPSender::create_path_message(IPAddress dest_addr, int dest_port) {
    click_chatter("RSVPSender -- Sending Path message towards %s", dest_addr.s().c_str());
    this->_dst = dest_addr;
    this->_port = dest_port;
    WritablePacket* packet = Packet::make(sizeof(click_ip)+sizeof(click_ether),0,sizeof(RSVPPath),0);
    RSVPPath *path = (RSVPPath*) packet->data();
    uint8_t version = 1;
    // COMMON HEADER
    path->_common.versionANDflags = version << 4; // Set to correct value: flags unused, version = 1 (first 4 bits, needs to be shifted)
    path->_common.type = 1;
    path->_common.checksum = 0; // TODO Calculate proper checksum needs to be fixed
    path->_common.send_TTL = 255; // TODO decide on TTL value
    path->_common.reserved = 0;
    path->_common.length = 0; // TODO calculate proper message length
    
    // INTEGRITY
    
    // SESSION HEADER
    path->_session._objHeader.length = (12 << 8);
    path->_session._objHeader.class_num = 1;
    path->_session._objHeader.c_type = 1;
    path->_session.dest_addr = dest_addr;
    path->_session.prot_id = 0x2E; // TODO needs to be non-zero. (determine protocol id)
    path->_session.flags = 0x01; // flag used by path message (source: rfc p.78)
    path->_session.destPort = dest_port; // TODO make sure destination port can be added if one exists
    
    // RSVP HOP
    path->_hop._objHeader.length = (12 << 8);
    path->_hop._objHeader.class_num = 3;
    path->_hop._objHeader.c_type = 1;
    path->_hop.prev_hop = _src; // TODO
    path->_hop.lih = 0; // TODO
    
    // TIME VALUES
    path->_tvalues._objHeader.length = (8 << 8);
    path->_tvalues._objHeader.class_num = 5;
    path->_tvalues._objHeader.c_type = 1;
    path->_tvalues.refresh_period = htonl(30000); // 30s default value
    
    // POLICY DATA
    
    // SENDER DESCRIPTION
    path->_sender._objHeader.length = (12 << 8);
    path->_sender._objHeader.class_num = 11;
    path->_sender._objHeader.c_type = 1;
    path->_sender.sender_addr = _src;
    path->_sender.sender_port = htons(_port);
    
    path->_obj.length = (36 << 8);
    path->_obj.class_num = 12;
    path->_obj.c_type = 2;
    
    path->_sender_Tspec.formatANDresv = 0;
    path->_sender_Tspec.overall_length = htons(7);
    path->_sender_Tspec.serv_header = 1;
    path->_sender_Tspec.resv = 0;
    path->_sender_Tspec.length_CLD = htons(6);
    path->_sender_Tspec.TB_TSpec = 127;
    path->_sender_Tspec.flags = 0;
    path->_sender_Tspec.param = htons(5);
    path->_sender_Tspec.TB_rate = htonl(0x47127c00); // 300 * 125
    path->_sender_Tspec.TB_size = htonl(0x441c4000); // 625
    path->_sender_Tspec.peak_data_rate = 0;
    path->_sender_Tspec.min_pol_unit = 0;
    path->_sender_Tspec.max_packet_size = htonl(150);  
    
    path->_common.length = (packet->length() << 8);
    path->_common.checksum = click_in_cksum((unsigned char *)path, sizeof(RSVPPath));
    
    
    srand (time(NULL));
    double timeoutValue = (double) rand() / INT_MAX + 0.5;
    
    _pathTimeout->schedule_after_msec(timeoutValue * 30000.0);
    
    output(0).push(packet);
}

void RSVPSender::create_path_tear_message(IPAddress dest_addr, int dest_port) {
    WritablePacket* packet = Packet::make(sizeof(click_ip)+sizeof(click_ether), 0, sizeof(RSVPPathTear), 0);
    RSVPPathTear *ptear = (RSVPPathTear*) packet->data();
    
    // COMMON HEADER
    ptear->_common.versionANDflags = 0; // TODO Set correct value (see create_path_message)
    ptear->_common.type = 5;
    ptear->_common.checksum = 0; // TODO Calculate checksum
    ptear->_common.send_TTL = 0; // TODO Determine appropriate time to live
    ptear->_common.reserved = 0;
    ptear->_common.length = 0;
    
    // INTEGRITY
    
    // SESSION HEADER
    ptear->_session._objHeader.length = (12 << 8);
    ptear->_session._objHeader.class_num = 1;
    ptear->_session._objHeader.c_type = 1;
    ptear->_session.dest_addr = dest_addr;
    ptear->_session.prot_id = 0x2E; // TODO needs to be non-zero (protocol id = ?)
    ptear->_session.flags = 0X01; // Needs to be disabled in first node that can determine effective edge in network
    ptear->_session.destPort = dest_port; // TODO make sure destination port is added if one exists
    
    // RSVP HOP
    ptear->_hop._objHeader.length = (12 << 8);
    ptear->_hop._objHeader.class_num = 3;
    ptear->_hop._objHeader.c_type = 1;
    ptear->_hop.prev_hop = _src; // TODO
    ptear->_hop.lih = 0; // TODO
    
    // SENDER DESCRIPTION
    ptear->_sender._objHeader.length = (12 << 8);
    ptear->_sender._objHeader.class_num = 11;
    ptear->_sender._objHeader.c_type = 1;
    ptear->_sender.sender_addr = _src;
    ptear->_sender.sender_port = _port;
    
    
    ptear->_common.length = (packet->length() << 8);
    ptear->_common.checksum = click_in_cksum((unsigned char *)ptear, sizeof(RSVPPathTear));
    output(0).push(packet);
}

CLICK_ENDDECLS
EXPORT_ELEMENT(RSVPSender)
