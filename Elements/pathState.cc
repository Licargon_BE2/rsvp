#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/ether.h>
#include <clicknet/udp.h>

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#include "rsvpheaders.hh"
#include "pathState.hh"

CLICK_DECLS


int PathState::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, "PUBLIC_IP", cpkM, cpIPAddress, &_pubIP, "PRIVATE_IP", cpkM, cpIPAddress, &_privIP, cpEnd) < 0) return -1;
    pathTimeout = new Timer(this);
    pathTimeout->initialize(this);
    this->_prevHop = 0;
    return 0;
}

void PathState::run_timer(Timer* t){
}

// Incoming packet from port 0: From client to internet, so give public as prev hop
// Incoming packet from port 1: From internet to client_network, so give private as prev hop

void PathState::push(int port, Packet* p){
    bool error = false;
    int headroom = sizeof(click_ip) + sizeof(click_ether);
    common_header* common = (common_header*) (p->data() + headroom);
    if (common->type == 1){ // Path message
	int currentPosition = 0; // Current position where we are in the packet
	
	while (currentPosition < ((common->length >> 8) - sizeof(common_header))){
	    object_header* oh = (object_header*) (p->data() + headroom + sizeof(common_header) + currentPosition);
	    int cl = (int) oh->class_num; // Cast to int to make switch statement work
	    if (cl == 3){
		rsvp_hop* hop = (rsvp_hop*) oh;
		this->_prevHop = IPAddress(hop->prev_hop);
		//click_chatter("Prev hop: %s", prevHop.s().c_str());
		
		if (port == 0){ // To internet, give public address
		    //click_chatter("Packet coming from client, giving public IP %s", this->_pubIP.s().c_str());
		    hop->prev_hop = this->_pubIP;
		} else {
		    //click_chatter("Packet coming from internet, giving private IP %s", this->_privIP.s().c_str());
		    hop->prev_hop = this->_privIP;
		}
	    } else if (cl == 5){ // Time values, set timeout
		time_values* tv = (time_values*) oh;
		//click_chatter("Time value: %d", ntohl(tv->refresh_period));
		int refresh_period = ntohl(tv->refresh_period);
		// Refresh timer randomly chosen in interval [0.5R, 1.5R]
		//srand (time(NULL));
		//double timeoutValue = (double) rand() / INT_MAX + 0.5;
		//pathTimeout->schedule_after_msec(timeoutValue * 30000.0);
		// (K + 0.5) * 1.5 * R
		double timeoutValue = 3.5 * 1.5 * ((double) refresh_period);
		pathTimeout->schedule_after_msec(timeoutValue);
	    }
		
	    currentPosition += (oh->length >> 8);	    
	}
    } else if (common->type == 2){ // Resv message
	if (this->_prevHop == 0){
	    click_chatter("PathState -- ERROR: No path state found. Sending ResvErr.");
	    output(4).push(p);
	    return;
	    error = true;
	} else {
	    WritablePacket* wp = p->uniqueify();
	    click_ip* iph = wp->ip_header();
	    //click_chatter("Rewriting current dest %s to %s", IPAddress(iph->ip_dst).s().c_str(), this->_prevHop.s().c_str());
	    iph->ip_dst = this->_prevHop.in_addr();
	    output(port + 2).push(wp);
	    return;
	}
    } else if (common->type == 5){ // Path Tear, delete state
	click_chatter("PathState -- Path tear received on port %d", port);
	this->_prevHop = 0;
    } else if (common->type == 6){ // Resv Tear, forward to our classifierthingy
	click_chatter("PathState -- Resv tear received on port %d", port);
	// TODO: Output to new port, link to classifier/tokenbucketthingamajig
    }
    
    if (!error) output(port).push(p);
    return;
}

CLICK_ENDDECLS
EXPORT_ELEMENT(PathState)

