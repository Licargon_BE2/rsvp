#ifndef CLICK_PROCESSERROR_HH
#define CLICK_PROCESSERROR_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>

CLICK_DECLS

// Element that accepts & reads out path messages. Nothing more atm.

class ProcessError: public Element {
public:
    ProcessError() {};
    const char *class_name() const { return "ProcessError";}
    const char *port_count() const { return "1/1"; }
    const char *processing() const { return PUSH; } 
    int configure (Vector<String> &, ErrorHandler *);    
    void push(int, Packet*);
};
CLICK_ENDDECLS
#endif