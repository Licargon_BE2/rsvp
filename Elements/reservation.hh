#ifndef CLICK_RESERVATION_HH
#define CLICK_RESERVATION_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/timer.hh>

CLICK_DECLS

class Reservation: public Element {
public:
    Reservation() {};
    const char *class_name() const { return "Reservation";}
    const char *port_count() const { return "2/2"; }
    const char *processing() const { return PUSH; } 
    int configure (Vector<String> &, ErrorHandler *);    
    void push(int, Packet*);

};
CLICK_ENDDECLS
#endif