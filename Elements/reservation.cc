#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/ether.h>
#include <clicknet/udp.h>

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#include "rsvpheaders.hh"
#include "reservation.hh"

CLICK_DECLS

int Reservation::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    return 0;
}

void Reservation::push(int port, Packet* p){
    output(port).push(p);
}

CLICK_ENDDECLS
EXPORT_ELEMENT(Reservation)

