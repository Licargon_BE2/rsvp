//
//  SetIPTos.hh
//  
//
//  Created by Nicolas Demarbaix
//
//  Header file for setting the tos in an ip packet

#ifndef CLICK_SETIPTOS_HH
#define CLICK_SETIPTOS_HH

#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>

CLICK_DECLS

class SetIPTos : public Element {
public:
    SetIPTos() {};
    ~SetIPTos() {};
    
    const char *class_name() const { return "SetIPTos"; }
    const char *port_count() const { return "1/2"; }
    const char *processing() const { return PUSH; }
    int initialize(ErrorHandler *errh);
    int configure(Vector<String>&, ErrorHandler*);
    void push(int, Packet*);
    
    // Handlers
    void add_handlers();
  
private:
    static int set_sp(const String &conf, Element *e, void *thunk, ErrorHandler *errh);
    
    int _sp; // Session port number
    
};

CLICK_ENDDECLS
#endif /* defined(CLICK_SETIPTOS_HH) */
