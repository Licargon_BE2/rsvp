#ifndef CLICK_PROCESSPATH_HH
#define CLICK_PROCESSPATH_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>

CLICK_DECLS

// Element that accepts & reads out path messages. Nothing more atm.

class ProcessPath: public Element {
public:
    ProcessPath() {};
    const char *class_name() const { return "ProcessPath";}
    const char *port_count() const { return "1/1"; }
    const char *processing() const { return PUSH; } 
    int configure (Vector<String> &, ErrorHandler *);    
    void push(int, Packet*);
};
CLICK_ENDDECLS
#endif