#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/ether.h>
#include <clicknet/udp.h>
#include "rsvpheaders.hh"
#include "processError.hh"

CLICK_DECLS

int ProcessError::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    return 0;
}

void ProcessError::push(int port, Packet* p){
	int headroom = sizeof(click_ip) + sizeof(click_ether);
	common_header* common = (common_header*) (p->data() + headroom);
	if (common->type == 3) click_chatter("----- PATH ERROR MESSAGE -----");
	if (common->type == 4) click_chatter("----- RESV ERROR MESSAGE -----");

	click_chatter("Total packet length: %d", (common->length >> 8));
	
	int currentPosition = 0; // Current position where we are in the packet
	
	while (currentPosition < ((common->length >> 8) - sizeof(common_header))){
	    object_header* oh = (object_header*) (p->data() + headroom + sizeof(common_header) + currentPosition);
	    int cl = (int) oh->class_num; // Cast to int to make switch statement work
	    switch (cl){ 
		case 1:
		    click_chatter("SESSION HEADER");
		    break;
		case 3:
		    click_chatter("HOP HEADER");
		    break; 
		case 4:
		    click_chatter("INTEGRITY HEADER");
		    break;
		case 5:
		    click_chatter("TIME HEADER");
		    break;
		case 6:
		    click_chatter("ERROR SPEC HEADER");
		    break;
		case 7:
		    click_chatter("SCOPE HEADER");
		    break;
		case 8:
		    click_chatter("STYLE HEADER");
		    break;
		case 9:
		    click_chatter("FLOW SPEC");
		    break;
		case 10:
		    click_chatter("FILTER SPEC");
		    break;
		case 11:
		    click_chatter("SENDER TEMPLATE HEADER");
		    break; 
		case 12:
		    click_chatter("SENDER TSPEC");
		    break;
		case 13:
		    click_chatter("POLICY DATA HEADER");
		    break;
		case 15:
		    click_chatter("RESV CONFIRM HEADER");
		    break;
		default:
		    click_chatter("Unknown type");
		    break;
	    }
	    currentPosition += (oh->length >> 8);	    
	}
	click_chatter("----- End of packet reached -----");
	output(0).push(p);
	return;
}

CLICK_ENDDECLS
EXPORT_ELEMENT(ProcessError)

