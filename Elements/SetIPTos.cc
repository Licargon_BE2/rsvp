//
//  SetIPTos.cc
//  
//
//  Created by Nicolas Demarbaix
//
//

#include <click/config.h>
#include <click/confparse.hh>
#include <click/element.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include "SetIPTos.hh"

CLICK_DECLS

int SetIPTos::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0)
        return -1;
    return 0;
}

int SetIPTos::initialize(ErrorHandler *errh){
}

int SetIPTos::set_sp(const String &conf, Element *e, void *thunk, ErrorHandler *errh) {
    SetIPTos *me = (SetIPTos*) e;
    int port;
    if (cp_va_kparse(conf, me, errh,"PORT", cpkM, cpInteger, &port, cpEnd) < 0) // Read session port
        return -1;
    
    // TODO discover true values for Type of Service byte, parse correctly
    me->_sp = port;
    return 0;
}

void SetIPTos::add_handlers() {
    add_write_handler("qos", &set_sp, (void*)0);
}

void SetIPTos::push(int, Packet* p) {
   WritablePacket *packet = p->uniqueify(); // Make writable copy of incoming packet
   click_ip *iph = packet->ip_header(); // Retrieve ip and udp header to check whether dstport is the same as given session port
   click_udp *udph = packet->udp_header();
   
   if (!udph) {
      output(1).push(packet);
      return;
   }

   if(htons(udph->uh_dport) == _sp) {
      iph->ip_tos = 184;
   }
   output(0).push(packet);
}

CLICK_ENDDECLS
EXPORT_ELEMENT(SetIPTos)
