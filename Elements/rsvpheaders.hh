#ifndef CLICK_RSVPHEADERS_H
#define CLICK_RSVPHEADERS_H

#include <stdint.h>

struct common_header {
	uint8_t versionANDflags;
	uint8_t type;
  	uint16_t checksum;
	uint8_t send_TTL;
	uint8_t reserved;
	uint16_t length;
};

struct object_header {
    uint16_t length;
    uint8_t class_num;
    uint8_t c_type;
};

struct session_header {
    object_header _objHeader;
    uint32_t dest_addr;
    uint8_t prot_id;
    uint8_t flags;
    uint16_t destPort;
};

struct rsvp_hop {
    object_header _objHeader;
    uint32_t prev_hop;
    uint32_t lih;
};

struct time_values{
    object_header _objHeader;
    uint32_t refresh_period;
};

struct error_spec{
    object_header _objHeader;
    uint32_t err_addr;
    uint8_t flags;
    uint8_t error_code;
    uint16_t error_value;
};

struct style {
    object_header _objHeader;
    uint8_t flags;
    uint8_t opt1;
    uint16_t opt2;
};

struct flow_spec{ 
    uint16_t formatANDresv;
    uint16_t overall_length;
    uint8_t serv_header;
    uint8_t resv;
    uint16_t length_CLD;
    uint8_t TB_TSpec; // 127, constant
    uint8_t flags; // None set
    uint16_t param; // Token bucket tspec length
    uint32_t TB_rate;
    uint32_t TB_size;
    uint32_t peak_data_rate;
    uint32_t min_pol_unit;
    uint32_t max_packet_size;
};

struct filter_spec { 
    object_header _objHeader;
    uint32_t src_addr;
    uint16_t nothing;
    uint16_t src_port;
};

struct resv_confirm {
    object_header _objHeader;
    uint32_t recv_addr;
};

struct sender_descriptor {
    object_header _objHeader;
    uint32_t sender_addr;
    uint16_t reserverd;
    uint16_t sender_port;
    //SENDER_TEMPLATE
    //SENDER_TSPEC
};

struct flow_descriptor {
    object_header _objHeader; 
    flow_spec _flow_spec;
    filter_spec _filter_spec;
};

struct RSVPPath {
    common_header _common;
    // include integrity
    session_header _session;
    rsvp_hop _hop;
    time_values _tvalues;
    // policy data
    sender_descriptor _sender;
    object_header _obj;
    flow_spec _sender_Tspec;
};

struct RSVPPathTear {
    common_header _common;
    // include integrity
    session_header _session;
    rsvp_hop _hop;
    sender_descriptor _sender;
};

struct RSVPPathErr {
    common_header _common;
    // include integrity
    session_header _session;
    error_spec _errspec;
    // include policy data
    sender_descriptor _sender;
};

struct RSVPResv {
    common_header _common;
    // include integrity
    session_header _session;
    rsvp_hop _hop;
    time_values _tvalues;
    // resv_confirm _rconfirm;
    // include scope
    // include policy data
    style _style;
    flow_descriptor _flow;
};

struct RSVPResvTear {
    common_header _common;
    // include integrity
    session_header _session;
    rsvp_hop _hop;
    // include scope
    style _style;
    flow_descriptor _flow;
};

struct RSVPResvErr {
    common_header _common;
    // include integrity
    session_header _session;
    rsvp_hop _hop;
    error_spec _errspec;
    // include scope
    // include policy data
    style _style;
    flow_descriptor _flow;
};

struct RSVPResvConfirm {
    common_header _common;
    // include integrity
    session_header _session;
    error_spec _errspec;
    resv_confirm _rconf;
    style _style;
    flow_descriptor _flow;
};

#endif
