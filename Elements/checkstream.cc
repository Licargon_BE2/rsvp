#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/ether.h>
#include <clicknet/udp.h>
#include "checkstream.hh"

CLICK_DECLS


int CheckStream::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    return 0;
}

void CheckStream::push(int port, Packet* p){
    WritablePacket *packet = p->uniqueify();
    click_ip *iph = packet->ip_header();
    
    if ((iph->ip_tos != 0)&& !forwarded) {
	click_chatter("***** IP TOS != 0");
	forwarded = true;
	output(1).push(packet);
    }
    output(0).push(p);
    
}

CLICK_ENDDECLS
EXPORT_ELEMENT(CheckStream)