#ifndef CLICK_CHECKSTREAM_HH
#define CLICK_CHECKSTREAM_HH

#include <click/config.h>
#include <click/confparse.hh>
#include <click/element.hh>
#include <click/error.hh>
#include <clicknet/ip.h>

CLICK_DECLS

/**
 * Checks input stream for possible QoS stream
 * If QoS stream detected, forward packet to RSVPSender as well to generate Path Msg
*/

class CheckStream : public Element {
public:
    CheckStream() {forwarded = false;};
    ~CheckStream() {};
    
    const char *class_name() const { return "CheckStream"; };
    const char *port_count() const {return "1/2"; };
    const char *processing() const {return PUSH; };
    
    int configure(Vector<String> &, ErrorHandler *);
    void push(int, Packet*);


private:
  bool forwarded;
};

CLICK_ENDDECLS
#endif