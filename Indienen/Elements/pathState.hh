#ifndef CLICK_PATHSTATE_HH
#define CLICK_PATHSTATE_HH
#include <click/config.h>
#include <click/element.hh>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/timer.hh>


CLICK_DECLS

// Element that accepts & reads out path messages. Nothing more atm.

// Input 0: eth0 (coming from client)
// Input 1: eth1 (coming from internet)

// Output 0 Path forward from client
// Output 1 Path forward from internet
// Output 2 Resv forward from client to internet
// Output 3 Resv forward from internet to client
// Output 4 Resv forward to RSVPError element to send ResvErr
class PathState: public Element {
public:
    PathState() {};
    const char *class_name() const { return "PathState";}
    const char *port_count() const { return "2/5"; }
    const char *processing() const { return PUSH; } 
    int configure (Vector<String> &, ErrorHandler *);    
    void push(int, Packet*);
    void run_timer(Timer* t);

private:
    IPAddress _pubIP;
    IPAddress _privIP;
    IPAddress _prevHop;
    Timer* pathTimeout;
};
CLICK_ENDDECLS
#endif