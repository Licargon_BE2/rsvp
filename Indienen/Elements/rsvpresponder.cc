//
//  rsvpresponder.cpp
//  
//
//  Created by Nicolas Demarbaix on 02/11/13.

#include <click/config.h>
#include <click/confparse.hh>
#include <click/element.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include "rsvpresponder.hh"

CLICK_DECLS

int RSVPResponder::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, "SOURCE", cpkM, cpIPAddress, &_src, cpEnd) < 0)
        return -1;
    this->_session_IP = "";
    return 0;
}

int RSVPResponder::resvHandler(const String &conf, Element *e, void * thunk, ErrorHandler* errh){
    RSVPResponder * me = (RSVPResponder *) e;
    if (cp_va_kparse(conf, me, errh, cpEnd) < 0) {
	click_chatter("RSVPResponder -- Something went wrong");
    	return -1;
    }
    //TODO FIX HANDLER ?? me->create_resv_message();
    return 0;
}

int RSVPResponder::resv_tearHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh) {
	RSVPResponder* me = (RSVPResponder*) e;
	if (cp_va_kparse(conf, me, errh, cpEnd) < -1) {
	    click_chatter("RSVPResponder -- Something went wrong");
	    return -1;
	}
	me->create_resv_tear_message();
	return 0;
}

void RSVPResponder::add_handlers() {
    add_write_handler("resv", &resvHandler, (void*)0);
    add_write_handler("resv_tear", &resv_tearHandler, (void*) 0);
}

void RSVPResponder::create_resv_message(IPAddress session_addr, int session_port, String srcAddress, int rate, int size, int m, int M) {
    WritablePacket *p = Packet::make(sizeof(click_ip)+sizeof(click_ether), 0, sizeof(RSVPResv), 0);
    RSVPResv *packet = (RSVPResv*) p->data();
    
    uint8_t version = 1;
    // COMMON HEADER
    packet->_common.versionANDflags = version << 4;
    packet->_common.type = 2;
    packet->_common.send_TTL = 255; // TODO decide on TTL value
    packet->_common.reserved = 0;
    
    // INTEGRITY ?
    
    // SESSION HEADER
    packet->_session._objHeader.length = (12 << 8);
    packet->_session._objHeader.class_num = 1;
    packet->_session._objHeader.c_type = 1;
    packet->_session.dest_addr = session_addr;
    packet->_session.prot_id = 11; // TODO needs to be non-zero. (determine protocol id)
    packet->_session.flags = 0; // flag used by path message (source: rfc p.78)
    packet->_session.destPort = htons(session_port); // TODO make sure destination port can be added if one exists
    
    // RSVP_HOP
    packet->_hop._objHeader.length = (12 << 8);
    packet->_hop._objHeader.class_num = 3; // TODO
    packet->_hop._objHeader.c_type = 1;
    packet->_hop.prev_hop = _src; // TODO
    packet->_hop.lih = 0; // TODO
    
    // TIME VALUES
    packet->_tvalues._objHeader.length = (8 << 8);
    packet->_tvalues._objHeader.class_num = 5; // TODO
    packet->_tvalues._objHeader.c_type = 1;
    packet->_tvalues.refresh_period = 0; // TODO calculate refresh timeout period (in ms)
    
    /*
    // RESV CONFIRM
    packet->_rconfirm._objHeader.length = (4 << 8);
    packet->_rconfirm._objHeader.class_num = 15;
    packet->_rconfirm._objHeader.c_type = 1;
    packet->_rconfirm.recv_addr = 0; // TODO adres invullen
    */
    
    // TODO SCOPE
    // TODO POLICY DATA
    
    // STYLE
    packet->_style._objHeader.length = (8 << 8);
    packet->_style._objHeader.class_num = 8;
    packet->_style._objHeader.c_type = 1;
    packet->_style.flags = 0;
    packet->_style.opt1 = 0;
    packet->_style.opt2 = 0x010b;
    
    // FLOW DESCRIPTOR
    
    packet->_flow._objHeader.length = (36 << 8); // TODO Increase incase of extra added data
    packet->_flow._objHeader.class_num = 9;
    packet->_flow._objHeader.c_type = 2;
    
    packet->_flow._flow_spec.formatANDresv = 0;
    packet->_flow._flow_spec.overall_length = htons(7);
    packet->_flow._flow_spec.serv_header = 5;
    packet->_flow._flow_spec.resv = 0;
    packet->_flow._flow_spec.length_CLD = htons(6);
    packet->_flow._flow_spec.TB_TSpec = 127;
    packet->_flow._flow_spec.flags = 0;
    packet->_flow._flow_spec.param = htons(5);
    /*packet->_flow._flow_spec.TB_rate = htonl(0x47127c00); // 300 * 125
    packet->_flow._flow_spec.TB_size = htonl(0x441c4000); // 625
    packet->_flow._flow_spec.peak_data_rate = 0;
    packet->_flow._flow_spec.min_pol_unit = 0;
    packet->_flow._flow_spec.max_packet_size = htonl(150);*/
    packet->_flow._flow_spec.TB_rate = rate; // 300 * 125
    packet->_flow._flow_spec.TB_size = size; // 625
    packet->_flow._flow_spec.peak_data_rate = 0;
    packet->_flow._flow_spec.min_pol_unit = m;
    packet->_flow._flow_spec.max_packet_size = M;
    
    packet->_flow._filter_spec._objHeader.length = (12 << 8);
    packet->_flow._filter_spec._objHeader.class_num = 10;
    packet->_flow._filter_spec._objHeader.c_type = 1;
    packet->_flow._filter_spec.src_addr = IPAddress(srcAddress).addr();
    packet->_flow._filter_spec.src_port = htons(session_port);
    
    packet->_common.length = (p->length() << 8);
    packet->_common.checksum = click_in_cksum((unsigned char *)packet, sizeof(RSVPResv));
 
    output(0).push(p);
}

void RSVPResponder::create_resv_tear_message() {
    WritablePacket *p = Packet::make(sizeof(click_ip)+sizeof(click_ether), 0, sizeof(RSVPResvTear), 0);
    RSVPResvTear *packet = (RSVPResvTear*) p->data();
    
    // COMMON HEADER
    packet->_common.versionANDflags = 1 << 4;
    packet->_common.type = 6;
    packet->_common.send_TTL = 255; // TODO decide on TTL value
    packet->_common.reserved = 0;
    
    // SESSION HEADER
    packet->_session._objHeader.length = (12 << 8);
    packet->_session._objHeader.class_num = 1;
    packet->_session._objHeader.c_type = 1;
    packet->_session.dest_addr = _src;
    packet->_session.prot_id = 11; // TODO needs to be non-zero. (determine protocol id)
    packet->_session.flags = 0x01; // flag used by path message (source: rfc p.78)
    packet->_session.destPort = 13337; // TODO make sure destination port can be added if one exists
    
    // RSVP_HOP
    packet->_hop._objHeader.length = (12 << 8);
    packet->_hop._objHeader.class_num = 3; // TODO
    packet->_hop._objHeader.c_type = 1;
    packet->_hop.prev_hop = _src; // TODO
    packet->_hop.lih = 0; // TODO
    
    // STYLE
    packet->_style._objHeader.length = (8 << 8);
    packet->_style._objHeader.class_num = 8;
    packet->_style._objHeader.c_type = 1;
    packet->_style.flags = 0;
    packet->_style.opt1 = 0; // First 19 reserved
    packet->_style.opt2 = 0; // TODO
    
    // FLOW DESCRIPTOR
    
    packet->_flow._objHeader.length = (4 << 8); // TODO Increase incase of extra added data
    packet->_flow._objHeader.class_num = 9;
    packet->_flow._objHeader.c_type = 2;
    // TODO: Add possible flow descriptors
    
    packet->_common.length = (p->length() << 8);
    packet->_common.checksum = click_in_cksum((unsigned char *)packet, sizeof(RSVPResvTear));
    
    output(0).push(p);
}

void RSVPResponder::push(int, Packet *p) {
    bool send_resv = false;
    int headroom = sizeof(click_ip) + sizeof(click_ether);
    common_header* common = (common_header*) (p->data() + headroom);

    // Check whether received packet is a RSVP Path message for which an automatic Resv message as reply needs to be generated
    if (common->type == 1) {
	send_resv = true;
    }
    
    int currentPosition = 0; // Current position where we are in the packet
    IPAddress session_addr;
    String src, dst;
    int dstPort;
    int tb_rate, tb_size, m, M;
    while (currentPosition < ((common->length >> 8) - sizeof(common_header))){
	object_header* oh = (object_header*) (p->data() + headroom + sizeof(common_header) + currentPosition);
	int cl = (int) oh->class_num; // Cast to int to make switch statement work
	if (cl == 1){
	    session_header* sh = (session_header*) oh;
	    session_addr = IPAddress(sh->dest_addr);
	    
	    if (session_addr.s() == this->_session_IP){ // Already session running, don't send a resv
		return;
	    } else {
		this->_session_IP = session_addr.s();
	    }
	    dst = IPAddress(sh->dest_addr).s().c_str();
	    dstPort = sh->destPort;
	} else if (cl == 11){
	    sender_descriptor* sender = (sender_descriptor*) oh;
	    src = IPAddress(sender->sender_addr).s().c_str();
	} else if (cl == 12){
	    flow_spec* fls = (flow_spec*) (oh + 1);
	    tb_size = fls->TB_size;
	    tb_rate = fls->TB_rate;
	    
	    m = fls->min_pol_unit;
	    M = fls->max_packet_size;
	}
	currentPosition += (oh->length >> 8);	    
    }
    
    // Send Resv message if required
    if (send_resv){
	click_chatter("RSVPResponder -- Sending back Resv message towards %s", session_addr.s().c_str());
	create_resv_message(session_addr, dstPort, src, tb_rate, tb_size, m, M);
    }
    
    return;
}

CLICK_ENDDECLS
EXPORT_ELEMENT(RSVPResponder)