//
//  rsvp_path.h
//  
//
//  Created by Nicolas Demarbaix on 02/11/13.
//
//  Header file for RSVP Sender element
//      > Used by source host to send Path messages et al.
//      > i.e. activate rsvp on path between host and destination

#ifndef CLICK_RSVPSENDER_HH
#define CLICK_RSVPSENDER_HH

#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <click/timer.hh>
#include "rsvpheaders.hh"

CLICK_DECLS

class RSVPSender : public Element {
public:
    RSVPSender() {};
    ~RSVPSender() {};
    
    const char *class_name() const { return "RSVPSender"; }
    const char *port_count() const { return "0/1"; }
    const char *processing() const { return PUSH; }
    int initialize(ErrorHandler *errh);
    int configure(Vector<String>&, ErrorHandler*);
    void run_timer(Timer* t);
    
    // Handlers
    void add_handlers();
  
private:
    IPAddress   _src;
    IPAddress _dst;
    int _port;
    void create_path_message(IPAddress, int);
    void create_path_tear_message(IPAddress, int);
    static int path(const String& conf, Element *e, void *thunk, ErrorHandler *errh);
    static int path_tear(const String &conf, Element *e, void *thunk, ErrorHandler *errh);
    Timer* _pathTimeout;
    
    
    
};

CLICK_ENDDECLS
#endif /* defined(CLICK_RSVPSender_HH) */
