//
//  rsvperror.cpp
//  
//
//  Created by Nicolas Demarbaix on 03/11/13.
//
//

#include <click/config.h>
#include <click/confparse.hh>
#include <click/element.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/icmp.h>
#include <clicknet/udp.h>
#include <clicknet/ether.h>
#include "rsvperror.hh"

CLICK_DECLS

int RSVPError::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0)
        return -1;
    
    return 0;
}

void RSVPError::push(int port, Packet* p){
    const click_ip* iph = p->ip_header();
    IPAddress src = IPAddress(iph->ip_src);
    IPAddress dst = IPAddress(iph->ip_dst);
    create_resv_error_message(dst, src); // Invert src and dst because it has to go back to the receiver.
}

void RSVPError::add_handlers() {
    add_write_handler("path_error", &path_error, (void*) 0);
    add_write_handler("resv_error", &resv_error, (void*) 0);
}

int RSVPError::path_error(const String &conf, Element *e, void *thunk, ErrorHandler *errh) {
    RSVPError *me = (RSVPError*) e;
    IPAddress src;
    IPAddress dst;
    if (cp_va_kparse(conf, me, errh,"SOURCE", cpkM, cpIPAddress, &src, "DEST", cpkM, cpIPAddress, &dst, cpEnd ) < 0) { // TODO Add SOURCE and DEST parameter for message routing
        click_chatter("Error while parsing config string for path_error handler");
        return -1;
    }
    
    me->create_path_error_message(src, dst);
    return 0;
}

int RSVPError::resv_error(const String &conf, Element *e, void *thunk, ErrorHandler *errh) {
    RSVPError *me = (RSVPError*) e;
    IPAddress src;
    IPAddress dst;
    if (cp_va_kparse(conf, me, errh, "SOURCE", cpkM, cpIPAddress, &src, "DEST", cpkM, cpIPAddress, &dst, cpEnd ) < 0) { // TODO Add SOURCE and DEST parameter for message routing
        click_chatter("Error while parsing config string for resv_error handler");
        return -1;
    }
    
    me->create_resv_error_message(src, dst);
    return 0;
}

void RSVPError::create_path_error_message(IPAddress src, IPAddress dst) {
    WritablePacket *packet = Packet::make(sizeof(click_ip)+sizeof(click_ether), 0, sizeof(RSVPPathErr), 0);
    RSVPPathErr *pErr = (RSVPPathErr*) packet->data();
    
    // COMMON HEADER
    pErr->_common.versionANDflags = 1 << 4;
    pErr->_common.type = 3;
    pErr->_common.checksum = 0;
    pErr->_common.send_TTL = 255;
    pErr->_common.reserved = 0;
    pErr->_common.length = 0;
    
    // INTEGRITY
    
    // SESSION HEADER
    pErr->_session._objHeader.length = 12 << 8;
    pErr->_session._objHeader.class_num = 1;
    pErr->_session._objHeader.c_type = 1;
    pErr->_session.dest_addr = dst;
    pErr->_session.prot_id = 0x2E;
    pErr->_session.flags = 0x01;
    pErr->_session.destPort = 0;
    
    // ERROR SPEC HEADER
    pErr->_errspec._objHeader.length = 12 << 8;
    pErr->_errspec._objHeader.class_num = 6;
    pErr->_errspec._objHeader.c_type = 1;
    pErr->_errspec.err_addr = src;
    pErr->_errspec.flags = 0;
    pErr->_errspec.error_code = 0;
    pErr->_errspec.error_value = 0;
    
    // POLICY DATA
    
    // SENDER DESCRIPTOR
    pErr->_sender._objHeader.length = (12 << 8);
    pErr->_sender._objHeader.class_num = 11;
    pErr->_sender._objHeader.c_type = 1;
    pErr->_sender.sender_addr = src;
    pErr->_sender.sender_port = 0;
    
    pErr->_common.length = (packet->length() << 8);
    pErr->_common.checksum = click_in_cksum((unsigned char *)pErr, sizeof(RSVPPathErr));
    
    output(0).push(packet);
}

void RSVPError::create_resv_error_message(IPAddress src, IPAddress dst) {
    WritablePacket *packet = Packet::make(sizeof(click_ip)+sizeof(click_ether), 0, sizeof(RSVPResvErr), 0);
    RSVPResvErr *rErr = (RSVPResvErr*) packet->data();
    
    // COMMON HEADER
    rErr->_common.versionANDflags = 1 << 4;
    rErr->_common.type = 4;
    rErr->_common.checksum = 0;
    rErr->_common.send_TTL = 255;
    rErr->_common.reserved = 0;
    rErr->_common.length;
    
    // INTEGRITY
    
    // SESSION HEADER
    rErr->_session._objHeader.length = (12 << 8);
    rErr->_session._objHeader.class_num = 1;
    rErr->_session._objHeader.c_type = 1;
    rErr->_session.dest_addr = dst;
    rErr->_session.prot_id = 0x2E;
    rErr->_session.flags = 0x01;
    rErr->_session.destPort = 0;
    
    // RSVP HOP HEADER
    rErr->_hop._objHeader.length = (12 << 8);
    rErr->_hop._objHeader.class_num = 3;
    rErr->_hop._objHeader.c_type = 1;
    rErr->_hop.prev_hop = 0; // TODO
    rErr->_hop.lih = 0; // TODO
    
    // ERROR SPEC HEADER
    rErr->_errspec._objHeader.length = 12 << 8;
    rErr->_errspec._objHeader.class_num = 6;
    rErr->_errspec._objHeader.c_type = 1;
    rErr->_errspec.err_addr = src;
    rErr->_errspec.flags = 0;
    rErr->_errspec.error_code = 0;
    rErr->_errspec.error_value = 0;
    
    // TODO SCOPE
    
    // TODO POLICY DATA
    
    // STYLE HEADER
    rErr->_style._objHeader.length = 8 << 8;
    rErr->_style._objHeader.class_num = 8;
    rErr->_style._objHeader.c_type = 1;
    rErr->_style.flags = 0;
    rErr->_style.opt1 = 0;
    rErr->_style.opt2 = 0;
    
    // FLOW DESCRIPTOR    
    rErr->_flow._objHeader.length = (4 << 8); // TODO Increase incase of extra added data
    rErr->_flow._objHeader.class_num = 9;
    rErr->_flow._objHeader.c_type = 2;
    // TODO: Add possible flow descriptors 
    
    rErr->_common.length = (packet->length() << 8);
    rErr->_common.checksum = click_in_cksum((unsigned char *)rErr, sizeof(RSVPResvErr));
    
    output(0).push(packet);
}

CLICK_ENDDECLS
EXPORT_ELEMENT(RSVPError)
