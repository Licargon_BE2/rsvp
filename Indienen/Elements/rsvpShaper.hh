#ifndef CLICK_RSVPSHAPER_HH
#define CLICK_RSVPSHAPER_HH

#include <click/config.h>
#include <click/confparse.hh>
#include <click/element.hh>
#include <click/error.hh>

#include <click/tokenbucket.hh>

#include <clicknet/ip.h>
#include "rsvpheaders.hh"

CLICK_DECLS

/**
 * Input 0: Standard packets
 * Input 1: Resv packet to set up filter spec
 * Input 2: ResvTear
 * 
 * Output 0: QoS stream compliant to TB
 * Output 1: QoS stream not compliant to TB AND BE stream
*/

class RSVPShaper : public Element {
public:
    RSVPShaper() {};
    ~RSVPShaper() {};
    
    const char *class_name() const { return "RSVPShaper"; };
    const char *port_count() const {return "2/2"; };
    const char *processing() const {return PUSH; };
    
    int configure(Vector<String> &, ErrorHandler *);
    void push(int, Packet*);


private:
    // Filter spec data
    IPAddress* _sender;
    int _port;
    TokenBucket* _tb;
    int _m; // Min policed unit
    int _M; // Max packet size
    bool _token;
};

CLICK_ENDDECLS
#endif