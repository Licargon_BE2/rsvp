#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/ether.h>
#include <clicknet/udp.h>

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#include "rsvpheaders.hh"
#include "rsvpclassifier.hh"

/*
 * To be used after an IPClassifier that classifies RSVP from non RSVP packets based on IP Protocol. 
 * A check is still made to be sure that a packet that passes this element is indeed an RSVP packet of some sort.
 */

CLICK_DECLS


int RSVPClassifier::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    return 0;
}

/*
 * Incoming packet from port 0 are initially classified between RSVP and non-RSVP. Non-RSVP packets are discarded 
 *  	(as they already passed an IPClassifier on IP Protocol 46 (= RSVP) and should not reach this element.
 *
 * In a following stage packets are classified as following:
 * 	- Output 0 = Path
 * 	- Output 1 = Resv 
 * 	- Output 2 = PathError 
 * 	- Output 3 = ResvError
 * 	- Output 4 = PathTear
 * 	- Output 5 = ResvTear
 */ 

void RSVPClassifier::push(int port, Packet* p){
    bool error = false;
    int headroom = sizeof(click_ip) + sizeof(click_ether);
    common_header* common = (common_header*) (p->data() + headroom);
    if (common->type == 1){ // Path message
	output(0).push(p);
	return;
    } else if (common->type == 2){ // Resv message
	output(1).push(p);
	return;
    } else if (common->type == 3){ // PathErr
	output(2).push(p);
	return;
    } else if (common->type == 4){ // ResvErr
	output(3).push(p);
	return;
    } else if (common->type == 5){ // PathTear
	output(4).push(p);
	return;
    } else if (common->type == 6){ // ResvTear
	output(5).push(p);
	return;
    }
    
}

CLICK_ENDDECLS
EXPORT_ELEMENT(RSVPClassifier)