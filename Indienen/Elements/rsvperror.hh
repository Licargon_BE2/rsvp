//
//  rsvperror.h
//  
//
//  Created by Nicolas Demarbaix on 03/11/13.
//
//

#ifndef CLICK_RSVPERROR_HH
#define CLICK_RSVPERROR_HH

#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include "rsvpheaders.hh"

CLICK_DECLS

class RSVPError : public Element {
public:
    RSVPError() {};
    ~RSVPError() {};
    
    const char *class_name() const { return "RSVPError"; };
    const char *port_count() const {return "1/1"; };
    const char *processing() const {return PUSH; };
    void push(int, Packet*);
    
    int configure(Vector<String> &, ErrorHandler *);

    void add_handlers();

private:
    void create_path_error_message(IPAddress, IPAddress);
    void create_resv_error_message(IPAddress, IPAddress);
    static int path_error(const String &conf, Element *e, void *thunk, ErrorHandler *errh);
    static int resv_error(const String &conf, Element *e, void *thunk, ErrorHandler *errh);
};

CLICK_ENDDECLS
#endif /* defined(____rsvperror__) */
