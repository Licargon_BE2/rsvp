//
//  rsvpresponder.h
//  
//
//  Created by Nicolas Demarbaix on 02/11/13.
//
//

#ifndef CLICK_RSVPRESPONDER_HH
#define CLICK_RSVPRESPONDER_HH

#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include "rsvpheaders.hh"

CLICK_DECLS

class RSVPResponder : public Element {
public:
    RSVPResponder() {};
    ~RSVPResponder() {};
    
    const char *class_name() const { return "RSVPResponder"; };
    const char *port_count() const { return "1/1"; };
    const char *processing() const { return PUSH; };
    
    int configure(Vector<String>&, ErrorHandler*);
    void push(int, Packet*);
    
    //Handlers
    void add_handlers();
    int resv(String &conf, Element *e, void *thunk, ErrorHandler *errh);
    int resv_tear(String &conf, Element *e, void *thunk, ErrorHandler *errh);
    
    
private:
    IPAddress   _src;
    
    static int resvHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh);
    static int resv_tearHandler(const String &conf, Element *e, void * thunk, ErrorHandler * errh);
    void create_resv_message(IPAddress session_addr, int session_port, String srcAddress, int tb_rate, int tb_size, int m, int M);
    void create_resv_tear_message();
    
    String _session_IP;
    int _session_Port;
};

CLICK_ENDDECLS
#endif