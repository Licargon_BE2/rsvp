#include <click/config.h>
#include <click/confparse.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include <clicknet/ether.h>
#include <clicknet/udp.h>
#include "rsvpheaders.hh"
#include "rsvpShaper.hh"

CLICK_DECLS


int RSVPShaper::configure(Vector<String> &conf, ErrorHandler *errh) {
    if (cp_va_kparse(conf, this, errh, cpEnd) < 0) return -1;
    this->_tb = 0;
    this->_sender = 0;
    this->_token = false;
    return 0;
}

void RSVPShaper::push(int port, Packet* p){
    if (port == 0){
	const click_ip* iph = p->ip_header();
	if ((iph->ip_tos == 184) && (this->_sender != 0) && (IPAddress(iph->ip_src).s() == this->_sender->s())){
	    //click_chatter("Entering TB part with M = %u and TB size = %u", this->_M, this->_tb->size());
	    this->_tb->refill(); // Refill the Token Bucket to the right amount
	    if ((p->length() <= this->_M) && (p->length() <= this->_tb->size())){
		//click_chatter("RSVPShaper -- QoS packet allowed by TB");
		this->_tb->remove(p->length());
		output(0).push(p);
		this->_token = true;
		return;
	    } else {
		//click_chatter("RSVPShaper -- Packet too large for TB. Packet size %d TB size %d", p->length(), this->_tb->size());
		output(1).push(p);
		this->_token = false;
		return;
	    }
	    // Check T if enough tokens push 0, else push 1
	    
	} else { // All the rest: BE
	    output(0).push(p);
	    return;
	}
    } else if (port == 1){ // Resv setup packet
	
	int headroom = sizeof(click_ip) + sizeof(click_ether);
	common_header* common = (common_header*) (p->data() + headroom);
	if (common->type == 2){ // Resv message
	    //click_chatter("RSVPShaper -- RESV msg arrived on port %d", port);
	    // Look for the right header
	    int currentPosition = 0; 
	    while (currentPosition < ((common->length >> 8) - sizeof(common_header))){
		object_header* oh = (object_header*) (p->data() + headroom + sizeof(common_header) + currentPosition);
		int cl = (int) oh->class_num;
		if (cl == 10){ // Filter spec header found, extra data
		    filter_spec* fis = (filter_spec*) oh;
		    this->_sender = new IPAddress(fis->src_addr);
		    this->_port = ntohs(fis->src_port);
		    //click_chatter("RSVPShaper -- Filter spec: IP %s and port %d", this->_sender->s().c_str(), this->_port);
		} else if (cl == 9){ // Flowspec header
		    if (this->_tb == 0){ // Setup TB
			flow_spec* fls = (flow_spec*) oh;
			int rate = fls->TB_size;
			int size = fls->peak_data_rate;
			this->_m = fls->min_pol_unit;
			this->_M = fls->max_packet_size; 
			click_chatter("RSVPShaper -- Resources reserved.");
			this->_tb = new TokenBucket(rate, size); // Testvalues
		    }
		}
		currentPosition += (oh->length >> 8);
	    }
	}
    } else if (port == 2){ // ResvTear
	this->_sender = 0;
	this->_tb = 0;
	click_chatter("RSVPShaper -- Resv Tear received. Reservation cancelled.");
    }	
    output(1).push(p);
    return;
}

CLICK_ENDDECLS
EXPORT_ELEMENT(RSVPShaper)