//
//  rsvpclassifier.h
//  
//
//  Created by Nicolas Demarbaix.
//
//

#ifndef CLICK_RSVPCLASSIFIER_HH
#define CLICK_RSVPCLASSIFIER_HH

#include <click/config.h>
#include <click/confparse.hh>
#include <click/element.hh>
#include <click/error.hh>
#include <clicknet/ip.h>
#include "rsvpheaders.hh"

CLICK_DECLS
/*
 * 	- Output 0 = Path
 * 	- Output 1 = Resv 
 * 	- Output 2 = PathError 
 * 	- Output 3 = ResvError
 * 	- Output 4 = PathTear
 * 	- Output 5 = ResvTear
 */
class RSVPClassifier : public Element {
public:
    RSVPClassifier() {};
    ~RSVPClassifier() {};
    
    const char *class_name() const { return "RSVPClassifier"; };
    const char *port_count() const {return "1/6"; };
    const char *processing() const {return PUSH; };
    
    int configure(Vector<String> &, ErrorHandler *);
    void push(int, Packet*);


private:
};

CLICK_ENDDECLS
#endif /* defined(____rsvpclassifier__) */